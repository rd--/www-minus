module Www.Minus.Rss.News where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Data.Time {- time -}

import qualified Text.Html.Minus as H {- html-minus -}

import qualified Www.Minus.Common as Common {- www-minus -}
import qualified Www.Minus.Rss as Rss {- www-minus -}
import qualified Www.Minus.Time as Time {- www-minus -}

-- * List

{- | Apply 'not' to function at 'filter'.

>>> remove isDigit "0one2"
"one"

>>> remove even [1..10] == filter (not . even) [1..10]
True
-}
remove :: (a -> Bool) -> [a] -> [a]
remove f = filter (not . f)

{- | 'dropWhile' from both ends of a list.

>>> trim isSpace " string "
"string"
-}
trim :: (a -> Bool) -> [a] -> [a]
trim f = reverse . dropWhile f . reverse . dropWhile f

-- * Markdown

{- | Heuristic to check if a line is a markdown reference definition.

>>> md_ln_p "  [www]: http://w3.org"
True
-}
md_ln_p :: String -> Bool
md_ln_p s =
  case dropWhile isSpace s of
    i : j -> i == '[' && "]:" `isPrefixOf` (dropWhile (/= ']') j)
    _ -> False

-- * News

type MD = [String]

type N_Date = UTCTime
type N_Title = String
type N_Description = [String]
type N_Entry = ((N_Date, N_Title), N_Description)
type N_News = ([N_Entry], MD)
type N_Opt = (String, String, String, String, String)

-- | Type to transform entry markup.
type N_Markup = (String -> String)

-- > n_sep_h "1 january 2012 | New News"
n_sep_h :: String -> Maybe (N_Date, N_Title)
n_sep_h h =
  case Common.split_on " | " h of
    [d, t] -> case Time.parse_news_date d of
      Just d' -> Just (d', trim isSpace t)
      Nothing -> Nothing
    _ -> Nothing

-- > n_parse_hd "###" "### 1 january 2012 | New News"
n_parse_hd :: String -> String -> Maybe (N_Date, N_Title)
n_parse_hd pre h = stripPrefix pre h >>= n_sep_h

-- > s <- readFile "/home/rohan/ut/www-log/data/md/news.md"
-- > n_parse "## " s
--
-- > s <- readFile "/home/rohan/ut/www-ltr/data/md/news.md"
-- > n_parse "### " s
n_parse :: String -> String -> ([N_Entry], MD)
n_parse pre s =
  let (nw, md) = partition (not . md_ln_p) (lines s)
      e = remove null (Common.sep (isPrefixOf pre) nw)
      f x = case trim null x of
        t : _ : r -> fmap (\t' -> (t', r)) (n_parse_hd pre t)
        _ -> undefined
  in (mapMaybe f e, sort md)

n_dsc_md :: MD -> N_Description -> String
n_dsc_md md dsc = unlines (dsc ++ ["\n"] ++ md)

n_entry_md :: String -> MD -> N_Entry -> String
n_entry_md pre md ((d, t), dsc) =
  let h = concat [pre, " ", Time.format_news_date d, " | ", t]
      b = n_dsc_md md dsc
  in unlines [h, "", b]

pubDate_t :: N_Date -> H.Content
pubDate_t = Rss.pubDate . Time.format_news_date

n_item :: String -> N_Markup -> MD -> N_Entry -> H.Content
n_item base f md ((dt, tt), dsc) =
  let ln = base ++ "?n=" ++ Time.format_iso_8601_date dt
  in Rss.item
      [ Rss.title tt
      , Rss.link ln
      , pubDate_t dt
      , Rss.category [] "News"
      , Rss.guid_ln ln
      , Rss.description_cdata (f (n_dsc_md md dsc))
      ]

entry_by_date :: N_Date -> [N_Entry] -> Maybe N_Entry
entry_by_date d = find (\((d', _), _) -> d' == d)

entry_by_date_s :: String -> [N_Entry] -> Maybe N_Entry
entry_by_date_s d e = Time.parse_iso_8601_date d >>= flip entry_by_date e

n_rss :: N_Opt -> N_Markup -> N_News -> H.Element
n_rss (t, l, x, d, b) f (nw, md) =
  let cf =
        [ Rss.title t
        , Rss.link l
        , Rss.atom_link_url x
        , Rss.description d
        ]
  in Rss.rss "2.0" [Rss.channel (cf ++ map (n_item b f md) nw)]
