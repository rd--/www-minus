{-
-- | Simple-minded remote text file editor with git/darcs commit rules.
module Www.Minus.Cgi.Editor where

import Data.Maybe {- base -}
import Data.Word {- base -}
import System.Directory {- directory -}
import System.Exit {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Data.Time as T {- time -}
import qualified Network.CGI as C {- cgi -}

import qualified Text.Html.Minus as H {- html-minus -}

import qualified Www.Minus.Cgi as Cgi
import qualified Www.Minus.Io as Io

-- * Types

data Vcs_T = Darcs | Git
type Author = String
type Remote = String
type Vcs_P = (Author,Maybe Remote)
type Vcs = (Vcs_T,Vcs_P)
type Url = String
type Password = String
type Title = String
type Time = String
type Text = String
data Config = Config {cfg_vcs :: Maybe Vcs
                     ,cfg_url :: Url
                     ,cfg_pwd :: Maybe Password}

-- * Genera

maybe_act :: b -> (a -> IO b) -> Maybe a -> IO b
maybe_act r = maybe (return r)

maybe_cmd :: (a -> IO ExitCode) -> Maybe a -> IO ExitCode
maybe_cmd = maybe_act ExitSuccess

maybe_cons :: Maybe a -> [a] -> [a]
maybe_cons e l = maybe l (: l) e

-- | For 'Char' in range convert to 'Word8', else 'error'.
char_to_word8 :: Char -> Word8
char_to_word8 c =
    let n = fromEnum c
    in if n < 0 || n > 255 then error "char_to_word8" else fromIntegral n

-- | 'B.pack' of 'char_to_word8'.
b_from_string :: String -> B.ByteString
b_from_string = B.pack . map char_to_word8

-- | Delete carriage returns from 'String', the browser may insert these.
delete_cr_str :: String -> String
delete_cr_str = filter (/= '\r')

-- | Delete carriage returns from 'B.ByteString', the browser may insert these.
delete_cr_b :: B.ByteString -> B.ByteString
delete_cr_b = B.filter (/= (char_to_word8 '\r'))

-- * IO

-- | Argument to constructor is the current time.
output_html :: (String -> H.Element) -> IO ()
output_html f = do
  t <- T.getCurrentTime
  Cgi.utf8_html_output (H.renderHtml5_pp (f (show t)))

-- * HTML

std_header :: Title -> Maybe Url -> H.Content
std_header title redirect =
    let t = H.title [] [H.cdata title]
        r = case redirect of
              Just u -> Just (H.meta [H.http_equiv "refresh"
                                     ,H.content ("1;Url=" ++ u)])
              Nothing -> Nothing
    in H.head [] (H.meta [H.charset "utf-8"] : maybe_cons r [t])

std_html :: Title -> Time -> Maybe Url -> H.Content -> H.Element
std_html title time redirect c =
    let p = H.p [] [H.cdata (title ++ ": " ++ time)]
        b = H.body [] [p,c]
    in H.html [H.lang "en"] [std_header title redirect,b]

-- | A basic @HTML@ text editor, UTF-8 encoding.
text_editor :: FilePath -> Text -> Time -> H.Element
text_editor fn initial time =
    let f_a = [H.accept_charset "utf-8",H.method "post",H.enctype "multipart/form-data"]
        f = H.form f_a c
        t_a = [H.name "text",H.rows "30",H.cols "80"]
        c = [H.textarea t_a [H.cdata initial]
            ,H.input_hidden "file" fn
            ,H.input_submit "edit" "edit"]
    in std_html fn time Nothing f

-- * Replies

message_only :: Title -> String -> Time -> H.Element
message_only title message time =
    let c = H.p [] [H.cdata message]
    in std_html title time Nothing c

message_link :: String -> String -> String -> String -> H.Element
message_link title message link time =
    let l = H.p [] [H.a [H.href link] [H.cdata link]]
        c = H.p [] [H.cdata message,l]
    in std_html title time Nothing c

message_redirect :: String -> String -> String -> String -> H.Element
message_redirect title message link time =
    let l = H.p [] [H.a [H.href link] [H.cdata link]]
        c = H.p [] [H.cdata message,l]
    in std_html title time (Just link) c

std_reply :: Config -> String -> String -> IO ()
std_reply c t m = output_html (message_link t m (cfg_url c))

-- * Vcs

vcs_remote :: Vcs_P -> Maybe Remote
vcs_remote = snd

-- * Git

run_git :: [String] -> IO ExitCode
run_git p = system ("git " ++ unwords p ++ " &>/tmp/run_git")

git_add :: Vcs_P -> FilePath -> IO ExitCode
git_add _ f = run_git ["add",f]

git_commit :: Vcs_P -> FilePath -> IO ExitCode
git_commit (au,_) fn = do
  t <- T.getCurrentTime
  let m = "'online edit of " ++ fn ++ " at " ++ show t ++ "'"
  run_git ["commit"
          ,fn
          ,"-m",m
          ,"--author='" ++ au ++ "'"]

git_push :: Vcs_P -> IO ExitCode
git_push = maybe_cmd (\r -> run_git ["push","--all",r]) . vcs_remote

-- * Darcs

run_darcs :: [String] -> IO ExitCode
run_darcs a = system ("darcs " ++ unwords a ++ " &>/dev/null")

darcs_add :: Vcs_P -> FilePath -> IO ExitCode
darcs_add _ f = run_darcs ["add",f]

-- | Commit all changes to 'FilePath'.
darcs_commit :: Vcs_P -> FilePath -> IO ExitCode
darcs_commit (au,_) fn = do
  t <- T.getCurrentTime
  let m = "'edit: " ++ fn ++ " at: " ++ show t ++ "'"
  run_darcs ["record",fn
            ,"--all"
            ,"--patch-name",m
            ,"--author",au]

darcs_push :: Vcs_P -> IO ExitCode
darcs_push = maybe_cmd (\r -> run_darcs ["push","--all",r]) . vcs_remote

-- * Vcs

sel_vcs :: (Vcs_P -> t) -> (Vcs_P -> t) -> t -> Maybe Vcs -> t
sel_vcs d g n v =
    case v of
      Just (Darcs,p) -> d p
      Just (Git,p) -> g p
      Nothing -> n

vcs_add :: Maybe Vcs -> FilePath -> IO ExitCode
vcs_add = sel_vcs darcs_add git_add (\_ -> return ExitSuccess)

vcs_commit :: Maybe Vcs -> FilePath -> IO ExitCode
vcs_commit = sel_vcs darcs_commit git_commit (\_ -> return ExitSuccess)

vcs_push :: Maybe Vcs -> IO ExitCode
vcs_push = sel_vcs darcs_push git_push (return ExitSuccess)

-- * Vcs (via Config)

vcs_add_c :: Config -> FilePath -> IO ExitCode
vcs_add_c c = vcs_add (cfg_vcs c)

vcs_commit_c :: Config -> FilePath -> IO ExitCode
vcs_commit_c c = vcs_commit (cfg_vcs c)

vcs_push_c :: Config -> IO ExitCode
vcs_push_c = vcs_push . cfg_vcs

-- * Edit

edit_get :: FilePath -> IO ()
edit_get fn = do
  t <- C.liftIO (Io.read_file_utf8_or (Left "") fn)
  output_html (text_editor fn t)

-- | The file name must be ASCII, the text may be UTF-8.
edit_post :: Config -> Url -> IO ()
edit_post c ln = do
  f <- C.getInput "file" -- ASCII string
  t <- C.getInputFPS "text" -- Word8 vector
  let f' = fromMaybe "no_file" f
      t' = maybe (b_from_string "no_text") delete_cr_b t
      act = do createDirectoryIfMissing True (takeDirectory f')
               B.writeFile f' t'
               _ <- vcs_commit_c c f'
               vcs_push_c c
  x <- C.liftIO act
  output_html (message_link
               "edit_post"
               ("edit stored: " ++ show (f',x))
               ln)

edit_text :: String -> Config -> FilePath -> IO ()
edit_text q c f =
    case q of
      "GET" -> edit_get f
      "POST" -> edit_post c (cfg_url c)
      _ -> undefined

-- * Upload

-- dir = default directory,acc = accepted files
upload_entry :: String -> String -> String -> String -> H.Element
upload_entry dir acc title time =
    let a = [H.method "post"
            ,H.enctype "multipart/form-data"]
        f = H.form a c
        c = [H.cdata "file: "
            ,H.input_ty H.File [H.name "file",H.accept acc]
            ,H.br []
            ,H.cdata "directory: "
            ,H.input_ty H.Text [H.name "dir",H.value dir]
            ,H.br []
            ,H.input_submit "enter" "enter"]
    in std_html title time Nothing f

-- > upload_get "data/jpeg" ".jpeg"
upload_get :: String -> String -> IO ()
upload_get dir acc = output_html (upload_entry dir acc "upload_get")

upload_post :: Config -> IO ()
upload_post c = do
  dir <- C.getInput "dir" -- ASCII file name
  name <- C.getInputFilename "file" -- ASCII file name
  bytes <- C.getInputFPS "file" -- Word8 vector data
  let dir' = fromMaybe "no_dir" dir
      name' = fromMaybe "no_name" name
      bytes' = fromMaybe B.empty bytes
      pw = joinPath [dir',takeFileName name']
      pl = joinPath ["../",pw]
  _ <- C.liftIO (do B.writeFile pw bytes'
                    _ <- vcs_add_c c pw
                    vcs_commit_c c pw)
  output_html (message_link "upload_post" "upload successful" pl)

-- * Login/Pwd

-- | ID string for the login cookie.
login_cookie :: String
login_cookie = "secret_login_cookie"

cfg_pwd_nil :: Config -> String
cfg_pwd_nil = fromMaybe "" . cfg_pwd

validated :: Config -> C.CGI Bool
validated c = do
  k <- C.getCookie login_cookie
  case k of
    Just pwd -> return (pwd == cfg_pwd_nil c)
    Nothing -> return False

pwd_entry :: Cgi.Query -> String -> String -> H.Element
pwd_entry q title time =
    let a = [H.action (Cgi.q_encode q)
            ,H.method "post"
            ,H.enctype "multipart/form-data"]
        f = H.form a c
        c = [H.input_ty H.Password [H.name "pwd"]
            ,H.input_submit "enter" "enter"]
    in std_html title time Nothing f

login_get :: Cgi.Query -> IO ()
login_get q = output_html (pwd_entry q "login_get")

login_post :: Config -> Cgi.Query -> IO ()
login_post c q = do
  pwd <- C.getInput "pwd"
  let pwd' = fromMaybe "" pwd
      u = Cgi.q_encode q
      c_msg = message_redirect "login_post" "login succeeded" u
      w_msg = message_link "login_post" "login failed" u
      set_ck = C.setCookie (C.newCookie login_cookie pwd')
      wr = output_html
  if pwd' == cfg_pwd_nil c then set_ck >> wr c_msg else wr w_msg

logout_get :: Config -> IO ()
logout_get c = do
  C.deleteCookie (C.newCookie login_cookie "")
  output_html (message_link "logout_get" "logged out" (cfg_url c))

-- * Errors

unknown_request :: Cgi.Parameters -> IO ()
unknown_request (m,q) =
    let m' = "method = " ++ m
        q' = ";query = " ++ show q
        i = "illegal operation: " ++ m' ++ q'
    in output_html (message_only "unknown_request" i)

-}
