module Www.Minus.Rss where

import Text.Html.Minus {- html-minus -}

-- xml_1_0 :: String
-- xml_1_0 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

-- mk_attr :: String -> String -> Attr
-- mk_attr a = Attr (unqual a)

-- cdata :: String -> Content
-- cdata s = Text (CData CDataText s Nothing)

-- mk_element :: String -> [Attr] -> [Content] -> Content
-- mk_element nm z e = Elem (Element (unqual nm) z e Nothing)

mk_plain_element :: String -> String -> Content
mk_plain_element nm t = mk_element nm [] [cdata t]

mk_unesc_element :: String -> String -> Content
mk_unesc_element nm t = mk_element nm [] [cdata_raw t]

type Version = String

version :: String -> Attr
version = mk_attr "version"

xmlns_atom :: String -> Attr
xmlns_atom = mk_attr "xmlns:atom"

xmlns_atom_w3 :: Attr
xmlns_atom_w3 = xmlns_atom "http://www.w3.org/2005/Atom"

rss :: Version -> [Content] -> Element
rss v = mk_element_e "rss" [version v, xmlns_atom_w3]

channel :: [Content] -> Content
channel = mk_element "channel" []

title :: String -> Content
title = mk_plain_element "title"

link :: String -> Content
link = mk_plain_element "link"

atom_link :: [Attr] -> Content
atom_link = mk_empty_element "atom:link"

-- > atom_link_url "http://x.org/rss.xml"
atom_link_url :: String -> Content
atom_link_url u = atom_link [href u, rel "self", type_attr "application/rss+xml"]

description :: String -> Content
description = mk_plain_element "description"

description_unesc :: String -> Content
description_unesc = mk_unesc_element "description"

-- > xml_cdata "x" == "<![CDATA[x]]>"
xml_cdata :: String -> String
xml_cdata s = "<![CDATA[" ++ s ++ "]]>"

description_cdata :: String -> Content
description_cdata = description_unesc . xml_cdata

pubDate :: String -> Content
pubDate = mk_plain_element "pubDate"

item :: [Content] -> Content
item = mk_element "item" []

isPermaLink :: String -> Attr
isPermaLink = mk_attr "isPermaLink"

guid :: [Attr] -> String -> Content
guid z = mk_element "guid" z . return . cdata

guid_ln :: String -> Content
guid_ln = guid [isPermaLink "true"]

domain :: String -> Attr
domain = mk_attr "domain"

category :: [Attr] -> String -> Content
category z = mk_element "category" z . return . cdata

render :: Element -> String
render e = unlines [xml_1_0, pp_element_def e]

{-
let r = rss "2.0" [channel [title "channel name"
                           ,link "channel link"
                           ,description "channel description"
                           ,item [title "item title"
                                 ,description "item description"
                                 ,pubDate "Mon, 23 Jan 2012 14:14:09 +1100"
                                 ,guid_ln "item url (unique)"]]]
putStr$ render r
-}
