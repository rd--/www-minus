-- | Md = mark-down
module Www.Minus.Md where

import qualified GHC.IO.Encoding {- base -}
import qualified System.Directory {- directory -}
import qualified System.FilePath {- filepath -}
import qualified System.IO {- base -}
import qualified System.Process {- base -}

import qualified Www.Minus.Common as Common {- www-minus -}
import qualified Www.Minus.Io as Io {- www-minus -}

-- | CMARK_BINARY environment variable, else cmark.
cmark_binary_name :: IO String
cmark_binary_name = Common.get_env_def "cmark" "CMARK_BINARY"

{- | Run 'cmark_binary_name' over markdown string.

> md <- System.IO.readFile "/home/rohan/ut/inland/md/19.8.md"
> html <- cmark_binary_md_to_html "" md
> putStrLn html
-}
cmark_binary_md_to_html :: FilePath -> String -> IO String
cmark_binary_md_to_html bin_dir str = do
  nm <- cmark_binary_name
  GHC.IO.Encoding.setLocaleEncoding System.IO.utf8
  let fn = bin_dir System.FilePath.</> nm
  x <- if null bin_dir then return True else System.Directory.doesFileExist fn
  if x then System.Process.readProcess fn ["--unsafe"] str else return "BIN/CMARK?" -- "--unsafe"

-- | Transform @markdown@ to @HTML@.
md_to_html :: FilePath -> String -> IO String
md_to_html = cmark_binary_md_to_html

-- | Read UTF-8 file and run 'md_to_html'.
md_load_html :: FilePath -> FilePath -> IO String
md_load_html bin_dir fn = do
  md_str <- Io.read_file_utf8_or (Left "md_load_html") fn
  md_to_html bin_dir md_str

-- | Run 'md_load_html' and write UTF-8 HTML file, replacing extension with @.html@.
md_convert :: FilePath -> FilePath -> IO ()
md_convert bin_dir fn = do
  html_str <- md_load_html bin_dir fn
  Io.write_file_utf8 (System.FilePath.replaceExtension fn ".html") html_str
