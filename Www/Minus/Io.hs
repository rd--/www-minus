{- | Utf-8 Text Io.
  Must compile and work with Ghc 8.2.1 with only default libraries.
-}
module Www.Minus.Io where

import qualified System.Directory as D {- directory -}
import qualified System.FilePath as P {- filepath -}
import qualified System.IO as IO {- base -}

-- | Implementation via "System.IO".
read_file_utf8_sysio :: FilePath -> IO String
read_file_utf8_sysio fn = do
  h <- IO.openFile fn IO.ReadMode
  IO.hSetEncoding h IO.utf8
  str <- IO.hGetContents h
  length str `seq` IO.hClose h `seq` return str

{- | Read (strictly) a Utf-8 encoded text file.

> s <- read_file_utf8 "/home/rohan/ut/inland/md/16.0.md"
> putStrLn s
> import qualified Data.ByteString.Lazy.UTF8 as Utf
> length s == Utf.length (Utf.fromString s)
-}
read_file_utf8 :: FilePath -> IO String
read_file_utf8 = read_file_utf8_sysio

-- | 'read_file_utf8' or, if the indicated file doesn't exist, a default value or fall-back file.
read_file_utf8_or :: Either String FilePath -> FilePath -> IO String
read_file_utf8_or def f = do
  x <- D.doesFileExist f
  if x then read_file_utf8 f else either return read_file_utf8 def

-- | Implementation via "System.IO".
write_file_utf8_sysio :: FilePath -> String -> IO ()
write_file_utf8_sysio fn str = do
  h <- IO.openFile fn IO.WriteMode
  IO.hSetEncoding h IO.utf8
  IO.hPutStr h str
  IO.hClose h

write_file_utf8 :: FilePath -> String -> IO ()
write_file_utf8 = write_file_utf8_sysio

char_percent_decode :: String -> Char
char_percent_decode code =
  let charList = " !#$%&'()*+,/:;=?@[]"
      codeList = words "20 21 23 24 25 26 27 28 29 2A 2B 2C 2F 3A 3B 3D 3F 40 5B 5D"
  in case lookup code (zip codeList charList) of
      Just answer -> answer
      _ -> error ("char_percent_decode?: " ++ code)

{- | Simple percent decoder for Urls

>>> url_percent_decode "Why%20SuperCollider%3F"
"Why SuperCollider?"
-}
url_percent_decode :: String -> String
url_percent_decode str =
  case str of
    [] -> []
    '%' : c1 : c2 : str' -> char_percent_decode [c1, c2] : url_percent_decode str'
    c : str' -> c : url_percent_decode str'

-- * Directory

{- | Get only directory elements with indicated extension.

> read_dir_ext ".hs" "/home/rohan/sw/www-minus/Www/Minus"
-}
read_dir_ext :: String -> FilePath -> IO [FilePath]
read_dir_ext ext dir = do
  ps <- D.getDirectoryContents dir
  let f nm = P.takeExtension nm == ext
  return (filter f ps)

{-
import qualified Data.ByteString as B {- bytestring -}
import qualified Data.Text as T {- text -}
import qualified Data.Text.Encoding as T {- text -}

-- | Read (strictly) a UTF-8 encoded text file, implemented via "Data.Text".
--   'T.decodeUtf8' of 'B.readFile'.
read_file_utf8_text :: FilePath -> IO T.Text
read_file_utf8_text = fmap T.decodeUtf8 . B.readFile

-- | Write UTF8 string as file, via "Data.Text".
write_file_utf8_text :: FilePath -> String -> IO ()
write_file_utf8_text fn = B.writeFile fn . T.encodeUtf8 . T.pack
-}
