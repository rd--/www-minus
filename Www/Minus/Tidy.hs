module Www.Minus.Tidy where

import System.Exit {- base -}
import qualified System.Process as P {- process -}

import qualified Text.Html.Minus as H {- html-minus -}

import qualified Www.Minus.Io as W {- www-minus -}

-- * Html Tidy

tidy_inplace_sys :: FilePath -> (String, [String])
tidy_inplace_sys fn =
  ( "tidy"
  ,
    [ "-utf8"
    , "-indent"
    , "-modify"
    , "-wrap"
    , "0"
    , "-file"
    , "/dev/null"
    , fn
    ]
  )

-- | Run @tidy@ inplace over indicated file.
tidy_inplace_run :: FilePath -> IO ()
tidy_inplace_run fn = do
  let (cmd, arg) = tidy_inplace_sys fn
  e <- P.rawSystem cmd arg
  case e of
    ExitFailure n ->
      if n == 1 -- warnings are not errors...
        then return ()
        else error "tidy_inplace_run: tidy failed"
    ExitSuccess -> return ()

-- | 'W.write_file_utf8' of 'H.renderHtml5' and then 'tidy_inplace_run'.
write_html5_utf8_and_tidy :: FilePath -> H.Element -> IO ()
write_html5_utf8_and_tidy fn c = do
  W.write_file_utf8 fn (H.renderHtml5_def c)
  tidy_inplace_run fn
