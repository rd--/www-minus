module Www.Minus.Cgi where

import Data.List {- base -}
import Data.Maybe {- base -}
import qualified System.IO as IO {- base -}

import Control.Monad.IO.Class {- transformers -}

import qualified Text.Html.Minus as H {- html-minus -}

import qualified Www.Minus.Common as Common {- www-minus -}

-- * Query

type QKey = String
type QValue = String

-- | A 'Query' is a set of ('Key','Value') pairs.
type Query = [(QKey, QValue)]

-- | Lookup 'QKey' in 'Query', require uniqueness if present.
q_lookup :: QKey -> Query -> Maybe QValue
q_lookup k q =
  case filter ((== k) . fst) q of
    [] -> Nothing
    [(_, v)] -> Just v
    _ -> Nothing -- error "q_lookup: non-unique"

-- | Lookup variant that 'error's on failure.
q_require :: QKey -> Query -> QValue
q_require k = fromMaybe (error "q_require") . q_lookup k

-- | Lookup 'k' and provide 'd' if not at 'q'.
q_default :: QKey -> QValue -> Query -> QValue
q_default k d = fromMaybe d . lookup k

{- | Make CGI query from /(key,value)/ set.

>>> q_encode [("work","022"),("id","022-017")]
"?work=022&id=022-017"
-}
q_encode :: Query -> String
q_encode qr =
  if null qr
    then ""
    else
      let f (k, v) = k ++ "=" ++ v
      in '?' : intercalate "&" (map f qr)

-- | 'H.href' of 'q_encode'.
q_href :: Query -> H.Attr
q_href = H.href . q_encode

-- | 'H.a' of 'q_href'.
q_anchor :: Query -> String -> H.Content
q_anchor q c = H.a [q_href q] [H.cdata c]

-- | Delete 'QKey' entry from 'Query'.
q_remkey :: QKey -> Query -> Query
q_remkey k = filter ((/= k) . fst)

q_remkeys :: [QKey] -> Query -> Query
q_remkeys k = filter ((`notElem` k) . fst)

{-
-- * Query & Cookie

-- | Set cookies at /c/ with values from /q/.
cq_set :: C.MonadCGI m => [QKey] -> Query -> m ()
cq_set c =
    let f (k,v) = if k `elem` c
                  then C.setCookie (C.newCookie k v)
                  else return ()
    in mapM_ f

-- | Get values for cookies at /c/ as a 'Query'.
cq_get :: C.MonadCGI m => [QKey] -> m Query
cq_get c = do
  r <- mapM C.getCookie c
  let jn a = maybe Nothing (\b -> Just (a,b))
  return (catMaybes (zipWith jn c r))

-- | Set cookies at /c/ from /q/.  Return /q/ extended to include /c/
-- not present, if stored.
cq_stateful :: C.MonadCGI m => [QKey] -> Query -> m Query
cq_stateful c q = do
  cq_set c q
  r <- cq_get c
  return (unionBy ((==) `on` fst) q r)

-}

-- * Cgi

{- | Redirect

>>> cgi_redirect (307,"Temporary Redirect") "u"
"HTTP/1.1 307 Temporary Redirect\nLocation: u\n\n"
-}
cgi_redirect :: (Int, String) -> String -> String
cgi_redirect (k, txt) u = concat ["HTTP/1.1 ", show k, " ", txt, "\nLocation: ", u, "\n\n"]

cgi_redirect_301 :: String -> String
cgi_redirect_301 = cgi_redirect (301, "Moved Permanently")

cgi_redirect_307 :: String -> String
cgi_redirect_307 = cgi_redirect (307, "Temporary Redirect")

cgi_header :: String -> String -> String
cgi_header ct cs = "Content-type: " ++ ct ++ "; charset=" ++ cs ++ "\n\n"

cgi_html_utf8 :: String -> String
cgi_html_utf8 = (++) (cgi_header "text/html" "utf-8")

cgi_text_utf8 :: String -> String
cgi_text_utf8 = (++) (cgi_header "text/plain" "utf-8")

cgi_xml_utf8 :: String -> String
cgi_xml_utf8 = (++) (cgi_header "text/xml" "utf-8")

cgi_rss_utf8 :: String -> String
cgi_rss_utf8 = (++) (cgi_header "application/rss+xml" "utf-8")

utf8_text_output :: String -> IO ()
utf8_text_output = putStr . cgi_text_utf8

utf8_xml_output :: String -> IO ()
utf8_xml_output = putStr . cgi_xml_utf8

utf8_html_output :: String -> IO ()
utf8_html_output = putStr . cgi_html_utf8

utf8_rss_output :: String -> IO ()
utf8_rss_output = putStr . cgi_rss_utf8

cgi_request_method :: IO String
cgi_request_method = Common.get_env_def "GET" "REQUEST_METHOD"

cgi_query_string :: IO String
cgi_query_string = Common.get_env_def "" "QUERY_STRING"

cgi_http_user_agent :: IO String
cgi_http_user_agent = Common.get_env_def "" "HTTP_USER_AGENT"

cgi_content_type :: IO String
cgi_content_type = Common.get_env_def "" "CONTENT_TYPE"

cgi_content_length :: IO String
cgi_content_length = Common.get_env_def "" "CONTENT_LENGTH"

maybe_delete_question_mark :: String -> String
maybe_delete_question_mark str =
  case str of
    '?' : r -> r
    _ -> str

-- cgi_error :: String -> IO ()
-- cgi_error str = putStr (cgi_text_utf8 str) >> error str

{- | Parse query

>>> cgi_parse_query ""
[]

>>> cgi_parse_query "t"
[("t","")]

>>> cgi_parse_query "t=contact"
[("t","contact")]

>>> cgi_parse_query "t=invisible&e=sifting/README"
[("t","invisible"),("e","sifting/README")]
-}
cgi_parse_query :: String -> [(String, String)]
cgi_parse_query q =
  case maybe_delete_question_mark q of
    [] -> []
    _ -> map (Common.separate_at_def "" "=") (Common.split_on "&" q)

type Method = String
type Parameters = (Method, Query)
type DispatchM m = Parameters -> m ()
type Dispatch = DispatchM IO

cgi_main :: Dispatch -> IO ()
cgi_main f = do
  liftIO (IO.hSetEncoding IO.stdin IO.utf8)
  liftIO (IO.hSetEncoding IO.stdout IO.utf8)
  m <- liftIO cgi_request_method
  q <- liftIO cgi_query_string
  f (m, cgi_parse_query q)

-- * Headers

-- | Scan @User-Agent@ header for @Mobi@.  Default value for case of no such header.
header_ua_is_mobile :: Bool -> IO Bool
header_ua_is_mobile def = do
  ua <- cgi_http_user_agent
  return (if null ua then def else "Mobi" `isInfixOf` ua)
