-- | Fetch URL to local file, using wget or curl.
module Www.Minus.Fetch where

import Control.Monad {- base -}
import qualified System.Directory as D {- filepath -}
import qualified System.FilePath as F {- filepath -}
import qualified System.Process as P {- process -}

type Url = String
type User_Agent = String
type Sys_Cmd = (String, [String])
type Output_File = FilePath
type Url_Fetch = (Url, Output_File, Maybe User_Agent)

-- | 'P.rawSystem' of 'Sys_Cmd'
sys_cmd_run :: Sys_Cmd -> IO ()
sys_cmd_run (cmd, arg) = void (P.rawSystem cmd arg)

{- | 'sys_cmd_run' only if output file does not already exist.
Returns True if command is run, else false.
-}
sys_cmd_run_exists :: (Sys_Cmd, FilePath) -> IO Bool
sys_cmd_run_exists (c, o_fn) = do
  let o_dir = F.takeDirectory o_fn
  D.createDirectoryIfMissing True o_dir
  x <- D.doesFileExist o_fn
  when (not x) (print o_fn >> sys_cmd_run c)
  return (not x)

-- | If /h/ is True write headers to @/dev/stderr@.
url_fetch_to_sys_cmd_curl :: Bool -> Url_Fetch -> Sys_Cmd
url_fetch_to_sys_cmd_curl h (u, o, a) =
  let a' = maybe [] (\agent -> ["--user-agent", agent]) a
      o' = ["--output", o]
      h' = if h then ["--dump-header", "/dev/stderr"] else []
  in ("curl", u : concat [a', o', h'])

url_fetch_curl :: Bool -> Url_Fetch -> IO Bool
url_fetch_curl h v =
  let (_, o_fn, _) = v
  in sys_cmd_run_exists (url_fetch_to_sys_cmd_curl h v, o_fn)

-- | Oddly, use curl if there is a User_Agent, else wget.
url_fetch_to_sys_cmd :: Url_Fetch -> Sys_Cmd
url_fetch_to_sys_cmd v =
  case v of
    (u, o, Nothing) -> ("wget", [u, "-O", o])
    _ -> url_fetch_to_sys_cmd_curl False v

-- | Get Url if output file doesn't already exist.
url_fetch :: Url_Fetch -> IO Bool
url_fetch v =
  let (_, o_fn, _) = v
  in sys_cmd_run_exists (url_fetch_to_sys_cmd v, o_fn)
