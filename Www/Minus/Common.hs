module Www.Minus.Common where

import Data.Function {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import qualified System.Environment {- base -}

group_on :: Eq x => (a -> x) -> [a] -> [[a]]
group_on f = map (map snd) . groupBy ((==) `on` fst) . map (\x -> (f x, x))

collate_on_adjacent :: (Eq k, Ord k) => (a -> k) -> (a -> v) -> [a] -> [(k, [v])]
collate_on_adjacent f g =
  let h l = case l of
        [] -> error "collate_on_adjacent"
        l0 : _ -> (f l0, map g l)
  in map h . group_on f

uncollate :: [(k, [v])] -> [(k, v)]
uncollate = concatMap (\(k, v) -> zip (repeat k) v)

{- | Separate at sublist.

>>> separate_at "--" "a--b--c"
Just ("a","b--c")
-}
separate_at :: Eq a => [a] -> [a] -> Maybe ([a], [a])
separate_at x =
  let n = length x
      f lhs rhs =
        case uncons rhs of
          Nothing -> Nothing
          Just (rhs0, rhsN) ->
            if x == take n rhs
              then Just (reverse lhs, drop n rhs)
              else f (rhs0 : lhs) rhsN
  in f []

separate_at_err :: Eq a => [a] -> [a] -> ([a], [a])
separate_at_err x = fromMaybe (error "separate_at") . separate_at x

separate_at_def :: Eq a => [a] -> [a] -> [a] -> ([a], [a])
separate_at_def r x s = fromMaybe (s, r) (separate_at x s)

{- | Split on sublist

>>> import Data.List.Split

>>> let f s x r = let (p,q) = (split_on s x,splitOn s x) in (p,q,p==q,p==r)
>>> f "," "a,b,c" ["a","b","c"]
(["a","b","c"],["a","b","c"],True,True)

>>> f "," "a,b,c," ["a","b","c",""]
(["a","b","c",""],["a","b","c",""],True,True)

>>> f ".." "a..b...c....d.." ["a","b",".c","","d",""]
(["a","b",".c","","d",""],["a","b",".c","","d",""],True,True)

>>> f "&" "t=invisible&e=sifting/README" ["t=invisible","e=sifting/README"]
(["t=invisible","e=sifting/README"],["t=invisible","e=sifting/README"],True,True)

>>> f "\n\n" "a\nb\nc\n\nd\ne\n\f\n" ["a\nb\nc","d\ne\n\f\n"]
(["a\nb\nc","d\ne\n\f\n"],["a\nb\nc","d\ne\n\f\n"],True,True)
-}
split_on :: Eq a => [a] -> [a] -> [[a]]
split_on x =
  let n = length x
      f acc lhs rhs =
        case uncons rhs of
          Nothing -> reverse (reverse lhs : acc)
          Just (rhs0, rhsN) ->
            if x == take n rhs
              then f (reverse lhs : acc) [] (drop n rhs)
              else f acc (rhs0 : lhs) rhsN
  in f [] []

{- | Separate on predicate.

>>> sep Data.Char.isDigit "1a2b3c"
["","1a","2b","3c"]
-}
sep :: (a -> Bool) -> [a] -> [[a]]
sep sel_f =
  let f acc lhs rhs =
        case rhs of
          [] -> reverse (reverse lhs : acc)
          c : rhs' ->
            if sel_f c
              then f (reverse lhs : acc) [c] rhs'
              else f acc (c : lhs) rhs'
  in f [] []

at_may :: [a] -> Int -> Maybe a
at_may l x =
  case l of
    [] -> Nothing
    e : l' -> if x == 0 then Just e else at_may l' (x - 1)

get_env_def :: String -> String -> IO String
get_env_def v k = fmap (fromMaybe v) (System.Environment.lookupEnv k)
