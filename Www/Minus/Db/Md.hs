{- | A very particular way of storing sets of Md files in a 'Db'.
A 'Record' has an @id@ key and a @fmt@ key, and if the @fmt@ key
is @md@ then there is an Md file at @dir/id.md@.
-}
module Www.Minus.Db.Md where

import Data.Maybe {- base -}
import System.FilePath {- filepath -}

import qualified Www.Minus.Db.Plain as Plain {- www-minus -}
import qualified Www.Minus.Io as Io {- www-minus -}
import qualified Www.Minus.Md as Md {- www-minus -}

-- | (id_key,fmt_key)
type Keys = (Plain.Key, Plain.Key)

-- | (id,(md_text,html_text))
type Md_Data = (String, (String, String))

type Md_Set = [Md_Data]

load_md :: FilePath -> Keys -> FilePath -> Plain.Record -> IO (Maybe Md_Data)
load_md bin_dir (id_k, fmt_k) dir e =
  if Plain.record_lookup_uniq fmt_k e == Just "md"
    then
      let k = Plain.record_lookup_uniq_err id_k e
          nm = dir </> k <.> "md"
      in do
          md_str <- Io.read_file_utf8 nm
          html_str <- Md.cmark_binary_md_to_html bin_dir md_str
          return (Just (k, (md_str, html_str)))
    else return Nothing

{- | Load texts of all 'Record' at 'Db' with 'Key' of @md@.

> db <- Plain.db_load "/home/rohan/ut/www-spr/data/db.text"
> let p = "/home/rohan/ut/www-spr/r"
> m <- load_all_md "/home/rohan/opt/bin" ("id","format") p db
> (length m,map (length . fst . snd) m,map (length . snd . snd) m)
-}
load_all_md :: FilePath -> Keys -> FilePath -> Plain.Db -> IO Md_Set
load_all_md bin_dir k dir = fmap catMaybes . mapM (load_md bin_dir k dir)

with_md :: FilePath -> Keys -> FilePath -> Plain.Db -> (Md_Set -> a) -> IO a
with_md bin_dir k dir db f = fmap f (load_all_md bin_dir k dir db)
