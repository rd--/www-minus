-- | @key: value@ database, allows duplicate @key@s.
module Www.Minus.Db.Plain where

import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Www.Minus.Common as Common {- www-minus -}
import qualified Www.Minus.Io as Io {- www-minus -}

-- * Type

-- | (Record-Separator,Field-Separator,Entry-Separator)
type Sep = (String, String, String)

type Key = String
type Value = String
type Entry = (Key, [Value])
type Record = [Entry]
type Db = [Record]

sep_plain :: Sep
sep_plain = (['\n', '\n'], ['\n'], ": ")

{- | Record parse

>>> record_parse (";","=") "F=f/rec;E=au;C=A;K=P;K=Q"
[("F",["f/rec"]),("E",["au"]),("C",["A"]),("K",["P","Q"])]
-}
record_parse :: (String, String) -> String -> Record
record_parse (fs, es) =
  Common.collate_on_adjacent fst snd
    . mapMaybe (Common.separate_at es)
    . Common.split_on fs

record_lookup :: Key -> Record -> [Value]
record_lookup k = fromMaybe [] . lookup k

record_lookup_at :: (Key, Int) -> Record -> Maybe Value
record_lookup_at (k, n) = flip Common.at_may n . record_lookup k

record_has_key :: Key -> Record -> Bool
record_has_key k = isJust . lookup k

record_lookup_uniq :: Key -> Record -> Maybe Value
record_lookup_uniq k r =
  case record_lookup k r of
    [] -> Nothing
    [v] -> Just v
    _ -> error "record_lookup_uniq: non uniq"

record_lookup_uniq_def :: Value -> Key -> Record -> Value
record_lookup_uniq_def v k = fromMaybe v . record_lookup_uniq k

record_lookup_uniq_err :: Key -> Record -> Value
record_lookup_uniq_err k r =
  let err = error (show ("record_lookup_uniq", k, r))
  in fromMaybe err (record_lookup_uniq k r)

db_find :: (Key, Value) -> Db -> [Record]
db_find (k, v) = filter ((== (Just v)) . record_lookup_uniq k)

db_parse :: Sep -> String -> [Record]
db_parse (rs, fs, es) s =
  let r = Common.split_on rs s
  in map (record_parse (fs, es)) r

{-
import qualified Music.Theory.List as T {- hmt -}
db_sort :: [(Key,Int)] -> [Record] -> [Record]
db_sort k = T.sort_by_n_stage (map record_lookup_at k)
-}

db_load_utf8 :: Sep -> FilePath -> IO Db
db_load_utf8 sep = fmap (filter (not . null) . db_parse sep) . Io.read_file_utf8

db_load :: FilePath -> IO Db
db_load = db_load_utf8 sep_plain

{- | Record Pp

>>> record_pp (";","=") [("F",["f/rec.au"]),("C",["A"])]
"F=f/rec.au;C=A"
-}
record_pp :: (String, String) -> Record -> String
record_pp (fs, es) = intercalate fs . map (\(k, v) -> k ++ es ++ v) . Common.uncollate

db_store_utf8 :: Sep -> FilePath -> [Record] -> IO ()
db_store_utf8 (rs, fs, es) fn = Io.write_file_utf8 fn . intercalate rs . map (record_pp (fs, es))
