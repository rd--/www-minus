-- | Database as map.
module Www.Minus.Db.Map where

import qualified Data.Aeson as A {- aeson -}
import qualified Data.ByteString.Lazy as B {- bytestring -}
import Data.List {- base -}
import Data.List.Split {- split -}
import qualified Data.Map as M {- containers -}
import Data.Maybe {- base -}

-- * Types

type Key = String
type Value = String
type Entity = M.Map Key Value
type Id = String
type Db = M.Map Id Entity

-- * Entity

e_from_list :: [(Key, Value)] -> Entity
e_from_list = M.fromList

e_get_maybe :: Key -> Entity -> Maybe Value
e_get_maybe = M.lookup

e_has_key :: Key -> Entity -> Bool
e_has_key k = isJust . e_get_maybe k

e_get :: Key -> Entity -> Value
e_get k e =
  let err = error (show ("e_get", k, e))
  in fromMaybe err (e_get_maybe k e)

e_get_def :: Value -> Key -> Entity -> Value
e_get_def v k = fromMaybe v . e_get_maybe k

e_get_nil :: Key -> Entity -> Value
e_get_nil = e_get_def ""

-- | First look for /k/, if not present look for /k'/, then 'error'.
e_get_fallback :: (Key, Key) -> Entity -> Value
e_get_fallback (k, k') e = maybe (e_get k' e) id (e_get_maybe k e)

-- | 'True' if 'Key' exists and has 'Value' @\"true\"@.
e_bool :: Key -> Entity -> Bool
e_bool k = (== (Just "true")) . e_get_maybe k

{- | Separate 'Value' into a list at commas.
If 'Key' does not exist or is /nil/ return the empty list.
-}
e_list_on :: Char -> Key -> Entity -> [Value]
e_list_on c k = maybe [] (splitOn [c]) . e_get_maybe k

{- | /p/ matches /q/ if /p/ is a superset of /q/, ie. /p/ has all of
the associations in /q/ and perhaps some others.
-}
e_match :: Entity -> Entity -> Bool
e_match e =
  let f (k, v) = fmap (== v) (e_get_maybe k e)
  in all (== Just True) . map f . M.toList

e_all :: ((Key, Value) -> Bool) -> Entity -> Bool
e_all f = all f . M.toList

-- * Db

db_entities :: Db -> [Entity]
db_entities = M.elems

db_size :: Db -> Int
db_size = M.size

db_from_list :: Key -> [Entity] -> Db
db_from_list k = M.fromList . map (\e -> (e_get k e, e))

db_get :: Id -> Db -> Maybe Entity
db_get = M.lookup

db_identifiers :: Db -> [Id]
db_identifiers = M.keys

db_filter :: (Entity -> Bool) -> Db -> Db
db_filter = M.filter

db_filter_eq :: Key -> Value -> Db -> Db
db_filter_eq k v = db_filter (\e -> e_get k e == v)

db_find_by :: (Entity -> Bool) -> Db -> Maybe Entity
db_find_by f = find f . db_entities

-- | Find first record that has given association.
db_find_eq :: Key -> Value -> Db -> Maybe Entity
db_find_eq k v = find (\e -> e_get k e == v) . db_entities

db_select_by :: (Entity -> Bool) -> Db -> [Entity]
db_select_by f = db_entities . db_filter f

-- | Select all records that have given association.
db_select_eq :: Key -> Value -> Db -> [Entity]
db_select_eq k v = db_entities . db_filter_eq k v

-- | Select records that 'e_match' /p/.
db_match :: Entity -> Db -> [Entity]
db_match p = filter (flip e_match p) . db_entities

db_values :: Key -> Db -> [Value]
db_values k = mapMaybe (e_get_maybe k) . db_entities

db_values_uniq :: Key -> Db -> [Value]
db_values_uniq k = sort . nub . db_values k

db_key_set :: Db -> [Key]
db_key_set = nub . sort . concatMap (M.keys . snd) . M.toList

-- * IO

-- | Load 'Db' from 'FilePath'.
load_db_js :: Key -> FilePath -> IO Db
load_db_js k fn = do
  b <- B.readFile fn
  case A.decode b of
    Just m -> return (db_from_list k m)
    Nothing -> return M.empty

-- | Idiom over 'load_db_js'.
with_db_js :: Key -> FilePath -> (Db -> a) -> IO a
with_db_js k fn f = fmap f (load_db_js k fn)

-- * Plain

db_to_plain :: Db -> [[(Key, Value)]]
db_to_plain = map M.toList . db_entities

{-

let fn = "/home/rohan/ut/www-spr/data/db.js"
db <- load_db_js "id" fn
db_size db
db_identifiers db
db_key_set db
map (\k -> (k,db_values_uniq k db)) (db_key_set db)

let p = db_to_plain db
nub $ sort $ map length p

-}
