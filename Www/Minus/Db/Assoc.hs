-- | Database as association list.  Uniqueness of 'Keys' is not presumed.
module Www.Minus.Db.Assoc where

import qualified Data.Aeson as A {- aeson -}
import qualified Data.ByteString.Lazy as B {- bytestring -}
import Data.List {- base -}
import Data.List.Split {- split -}
import qualified Data.Map as M {- containers -}
import Data.Maybe {- base -}
import Safe {- safe -}

-- * Types

type Key = String
type Value = String
type Entity = [(Key, Value)]
type Db = [Entity]

-- * Entity

e_key_seq :: Entity -> [Key]
e_key_seq = map fst

e_has_key :: Key -> Entity -> Bool
e_has_key k = elem k . e_key_seq

{-
e_key_histogram :: Entity -> [(Key,Int)]
e_key_histogram = T.histogram . e_key_seq

e_has_duplicate_keys :: Entity -> Bool
e_has_duplicate_keys = any (> 0) . map snd . e_key_histogram
-}

e_lookup_by :: (Key -> Key -> Bool) -> Key -> Entity -> [Value]
e_lookup_by f c = map snd . filter (f c . fst)

e_lookup :: Key -> Entity -> [Value]
e_lookup = e_lookup_by (==)

e_lookup_at :: (Key, Int) -> Entity -> Maybe Value
e_lookup_at (c, n) = flip atMay n . e_lookup c

e_lookup_uniq :: Key -> Entity -> Maybe Value
e_lookup_uniq k e =
  case e_lookup k e of
    [] -> Nothing
    [v] -> Just v
    _ -> error "e_lookup_uniq: non uniq"

e_has_key_uniq :: Key -> Entity -> Bool
e_has_key_uniq k = isJust . e_lookup_uniq k

e_lookup_uniq_err :: Key -> Entity -> Value
e_lookup_uniq_err k e =
  let err = error (show ("e_lookup_uniq_err", k, e))
  in fromMaybe err (e_lookup_uniq k e)

e_lookup_uniq_def :: Value -> Key -> Entity -> Value
e_lookup_uniq_def v k = fromMaybe v . e_lookup_uniq k

-- * e_get

-- | Type specialised 'lookup'.
e_get_maybe :: Key -> Entity -> Maybe Value
e_get_maybe = lookup

-- | Erroring variant.
e_get :: Key -> Entity -> Value
e_get k e =
  let err = error (show ("e_get", k, e))
  in fromMaybe err (e_get_maybe k e)

e_get_def :: Value -> Key -> Entity -> Value
e_get_def = e_lookup_uniq_def

e_get_nil :: Key -> Entity -> Value
e_get_nil = e_get_def ""

{- | First look for /k/, if not present look for /k'/, then 'error'.

>>> e_get_fallback ("a","b") [("a","x"),("b","y")]
"x"

>>> e_get_fallback ("a","b") [("b","y")]
"y"
-}
e_get_fallback :: (Key, Key) -> Entity -> Value
e_get_fallback (k, k') e = maybe (e_lookup_uniq_err k' e) id (e_lookup_uniq k e)

{- | 'True' if 'Key' exists and has 'Value' @\"true\"@.

>>> e_bool "k" [("k","true")]
True

>>> e_bool "k" [("k","false")]
False

>>> e_bool "k" []
False
-}
e_bool :: Key -> Entity -> Bool
e_bool k = (== (Just "true")) . e_lookup_uniq k

{- | Separate 'Value' into a list at commas.  If 'Key' does
not exist or is /nil/ return the empty list.

>>> e_list_on ',' "k" [("k","a,b,c")]
["a","b","c"]

>>> e_list_on ',' "k" []
[]
-}
e_list_on :: Char -> Key -> Entity -> [Value]
e_list_on c k = maybe [] (splitOn [c]) . e_lookup_uniq k

{- | /p/ matches /q/ if /p/ is a superset of /q/, ie. /p/ has all of
the associations in /q/ and perhaps some others.

>>> let f = map return
>>> let p = zip (f "abc") (f "012")
>>> let q = zip (f "ac") (f "02")
>>> let r = zip (f "ac") (f "01")
>>> (e_match p q,e_match q p,e_match p r)
(True,False,False)
-}
e_match :: Entity -> Entity -> Bool
e_match e =
  let f (k, v) = fmap (== v) (e_lookup_uniq k e)
  in all (== Just True) . map f

-- * Db

-- | Find first record that has given association.
db_find :: Key -> Value -> Db -> Maybe Entity
db_find k v = find (\e -> e_lookup_uniq_err k e == v)

-- | Select all records that have given association.
db_filter :: Key -> Value -> Db -> Db
db_filter k v = filter (\e -> e_lookup_uniq_err k e == v)

-- | Select records that 'e_match' /p/.
db_match :: Entity -> Db -> Db
db_match p = filter (flip e_match p)

db_values :: Key -> Db -> [Value]
db_values k = concatMap (e_lookup k)

db_values_uniq :: Key -> Db -> [Value]
db_values_uniq k = sort . nub . db_values k

-- * IO

{- | Load 'Db' from 'FilePath'.

> let fn = "/home/rohan/ut/www-spr/data/db.js"
> load_db_js fn >>= return.length
-}
load_db_js :: FilePath -> IO Db
load_db_js fn = do
  b <- B.readFile fn
  case A.decode b of
    Just m -> return (map M.toList m)
    Nothing -> return []

-- | Idiom over 'load_db_js'.
with_db_js :: FilePath -> (Db -> a) -> IO a
with_db_js fn f = fmap f (load_db_js fn)
