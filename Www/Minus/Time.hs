-- | Time functions.
module Www.Minus.Time where

import Data.Time {- time -}

-- * Parser

parse_time :: TimeLocale -> String -> String -> Maybe UTCTime
parse_time = parseTimeM True

default_time_locale :: TimeLocale
default_time_locale = defaultTimeLocale

-- * ISO 8601 (Y_m_d)

iso_8601_date_format_str :: String
iso_8601_date_format_str = "%Y-%m-%d"

{- | Parse Iso-8601

>>> parse_iso_8601_date "2012-01-07"
Just 2012-01-07 00:00:00 UTC
-}
parse_iso_8601_date :: String -> Maybe UTCTime
parse_iso_8601_date = parse_time defaultTimeLocale iso_8601_date_format_str

{- | Format Is0-8601

>>> let Just t = parse_iso_8601_date "2012-01-07"
>>> format_iso_8601_date t
"2012-01-07"
-}
format_iso_8601_date :: UTCTime -> String
format_iso_8601_date = formatTime defaultTimeLocale iso_8601_date_format_str

-- * Rfc 822

parse_rfc_822_date :: String -> Maybe UTCTime
parse_rfc_822_date = parse_time defaultTimeLocale rfc822DateFormat

{- | Format Rfc-822

>>> let Just t = parse_rfc_822_date "Sat,  7 Jan 2012 00:00:00 UTC"
>>> format_rfc_822_date t
"Sat,  7 Jan 2012 00:00:00 UTC"
-}
format_rfc_822_date :: UTCTime -> String
format_rfc_822_date = formatTime defaultTimeLocale rfc822DateFormat

-- * The Guardian

-- | <https://www.theguardian.com/info/series/guardian-and-observer-style-guide>
news_format :: String
news_format = "%e %B %Y"

parse_news_date :: String -> Maybe UTCTime
parse_news_date = parse_time defaultTimeLocale news_format

{- | Format news

>>> let Just t = parse_news_date "7 January 2012"
>>> format_news_date t
" 7 January 2012"
-}
format_news_date :: UTCTime -> String
format_news_date = formatTime defaultTimeLocale news_format
