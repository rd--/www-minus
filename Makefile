all:
	ghc -O2 --make Www/Minus.hs
	chmod ugo+x py/editor.py py/upload.py

mk-cmd:
	echo "www-minus - nil"

clean:
	rm -Rf dist dist-newstyle *~
	rm -f Text/Html Text/XML
	find . -name '*.hi' -o -name '*.o' | xargs rm -f

remote-build:
	ssh rd@rohandrape.net "(cd sw/www-minus; make clean ln-local all)"

ln-local:
	rm -f Text/Html Text/XML
	mkdir -p Text
	ln -s $(HOME)/sw/html-minus/Text/Html Text/Html
	ln -s $(HOME)/opt/src/xml-1.3.9/Text/XML Text/XML

push-all:
	r.gitlab-push.sh www-minus

indent:
	fourmolu -i Www

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Www
