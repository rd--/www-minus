#!/usr/bin/env python

import cgi
import cgitb
import datetime
import io
import os.path
import subprocess
import sys

cgitb.enable()

# put string (not bytes) to stdout
def putstr (s) :
    sys.stdout.write(s)
    sys.stdout.flush()

def git_add_1 (f_fn) :
    subprocess.call(["git", "add", f_fn, "--quiet"])

def git_commit_1 (f_fn) :
    commit_msg = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
    subprocess.call(["git", "commit", f_fn, "-m", commit_msg, "--quiet"])

c_begin = "Content-type: text/html; charset=utf-8\n\n"

h_begin = """
<html>
<head>
<title>file upload</title>
</head>
<body>
"""

h_end = """
</body>
</html>
"""

# upload.cgi should be os.environ['SCRIPT_NAME']
h_upload_template = """
<form accept-charset="utf-8" method="post" enctype="multipart/form-data" action="upload.cgi">
filename:
<input type="file" name="f_filename" accept="%(acc)s" />
<br />
directory:
<input type="text" name="f_directory" value="%(dir)s" />
<br />
<input type="submit" name="f_enter" value="enter" />
</form>
"""

h_result_template = """
<p>fn=%(fn)s; file stored</p>
<p>dir=%(dir)s;</p>
"""

def upload_form (q_dir) :
    putstr(c_begin)
    putstr(h_begin)
    putstr(h_upload_template % {'dir': q_dir, 'acc': ".jpeg"})
    putstr(h_end)

def upload_store (fs) :
    f_fn = fs["f_filename"]
    f_dir = fs.getvalue("f_directory", "")
    f_path = os.path.join(f_dir, f_fn.filename)
    fp = file (f_path, "wb")
    while 1:
        data = f_fn.file.read(100000)
        if not data: break
        fp.write (data)
    fp.close()
    putstr(c_begin)
    putstr(h_begin)
    putstr(h_result_template % {'fn': f_fn.filename, 'dir': f_dir})
    putstr(h_end)
    git_add_1(f_path)
    git_commit_1(f_path)

def upload_main () :
    fs = cgi.FieldStorage()
    q_dir = fs.getvalue("d", "data/jpeg")
    if fs.has_key("f_filename") :
        upload_store(fs)
    else :
        upload_form(q_dir)

upload_main()
