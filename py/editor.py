#!/usr/bin/python3

import cgi
import cgitb
import datetime
import io
import os.path
import subprocess
import sys

cgitb.enable()

# set stdout to be UTF-8 (python >=3.7)
# sys.stdout.reconfigure(encoding='utf-8')

# set stdout to be UTF-8 (python <3.7)
sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)

def rem_cr (s) :
    return s.replace("\r","")

# put string (not bytes) to stdout
def putstr (s) :
    sys.stdout.write(s)
    sys.stdout.flush()

def git_commit_1 (f_fn) :
    commit_msg = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
    subprocess.call(["git", "commit", f_fn, "-m", commit_msg, "--quiet"])

c_begin = "Content-type: text/html; charset=utf-8\n\n"

h_begin = """
<html>
<head>
<title>text editor</title>
</head>
<body>
"""

h_end = """
</body>
</html>
"""

# editor.cgi should be os.environ['SCRIPT_NAME']
h_editor_template = """
<p>filename=%(fn)s</p>
<form accept-charset="utf-8" method="post" enctype="multipart/form-data" action="editor.cgi">
<textarea name="f_text" rows="30" cols="80">%(txt)s</textarea>
<br />
<input type="hidden" name="f_filename" value="%(fn)s" />
<input type="submit" name="f_edit" value="edit" />
</form>
"""

h_result_template = """
<p>fn=%(fn)s; file stored</p>
<pre>%(txt)s</pre>
"""

# q_ = query, f_ = form

def editor_form (fs,fn) :
    fp = io.open(fn, mode="r", encoding="utf-8")
    file_txt = fp.read()
    fp.close()
    putstr(c_begin)
    putstr(h_begin)
    putstr(h_editor_template % {'fn': fn, 'txt': file_txt})
    putstr(h_end)

def editor_store (fs) :
    f_fn = fs.getvalue("f_filename", "")
    if f_fn != "" and os.path.isfile(f_fn) :
        result_txt = fs.getvalue("f_text", "")
        f_fp = io.open(f_fn, mode="w", encoding="utf-8")
        f_fp.write(rem_cr(result_txt))
        f_fp.close()
        putstr(c_begin)
        putstr(h_begin)
        putstr(h_result_template % {'fn': f_fn, 'txt': result_txt})
        putstr(h_end)
        git_commit_1(f_fn)
    else :
        putstr('Content-type: text/html\n\n')
        putstr("Nothing to edit...")

# rel_dir is required since ModSecurity won't allow ../ in path names in queries (i.e. ?t=../md/about.md is not allowed)
def editor_main (rel_dir) :
    fs = cgi.FieldStorage()
    q_fn = rel_dir + fs.getvalue("t", "")
    if q_fn != "" and os.path.isfile(q_fn) :
        editor_form(fs,q_fn)
    else :
        editor_store(fs)

editor_main("")
